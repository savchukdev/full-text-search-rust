mod analysis;
mod document;
mod index;

use document::Document;
use flate2::read::GzDecoder;
use index::{Index, SearchType};
use serde::Serialize;
use std::io;
use std::time::Instant;
use std::{
    fs::File,
    io::{BufReader, Write},
};

const ARCHIVE_FILENAME: &str = "enwiki-latest-abstract.xml.gz";

fn main() {
    let index_filename = format!("{}{}", ARCHIVE_FILENAME, ".index");

    let index = match std::fs::File::open(&index_filename) {
        Ok(file) => {
            print!("Index file is found. Loading...");
            std::io::stdout().flush().ok();
            let start = Instant::now();

            match rmp_serde::from_read(io::BufReader::new(file)) {
                Ok(index) => {
                    let end = Instant::now();
                    println!("succeeded. {:?}", end - start);
                    index
                }
                Err(e) => {
                    println!("failed: {}. Exiting", e.to_string());
                    return;
                }
            }
        }
        Err(_) => {
            println!("No index file found. Reading original archive");
            let file = File::open(ARCHIVE_FILENAME).expect("Could not find the archive file");
            let decoder = GzDecoder::new(file);

            let mut xml_reader = fast_xml::Reader::from_reader(BufReader::new(decoder));
            let mut buf = Vec::new();

            enum CaptureNode {
                None,
                Title,
                Url,
                Description,
            }
            let mut capturing = CaptureNode::None;
            let mut title = String::new();
            let mut url = String::new();
            let mut description = String::new();
            let mut doc_id = 0;

            let mut index = Index::new();

            let start = Instant::now();

            println!("Parsing...");

            loop {
                match xml_reader.read_event(&mut buf) {
                    Ok(fast_xml::events::Event::Start(ref e)) => match e.name() {
                        b"title" => capturing = CaptureNode::Title,
                        b"url" => capturing = CaptureNode::Url,
                        b"abstract" => capturing = CaptureNode::Description,
                        _ => {}
                    },
                    Ok(fast_xml::events::Event::End(ref e)) => match e.name() {
                        b"doc" => {
                            let doc = Document::new(
                                doc_id,
                                title.clone(),
                                url.clone(),
                                description.clone(),
                            );

                            index.index_document(doc);

                            doc_id += 1;
                        }
                        _ => capturing = CaptureNode::None,
                    },
                    Ok(fast_xml::events::Event::Text(ref e)) => match capturing {
                        CaptureNode::None => {}
                        CaptureNode::Title => {
                            title = e.unescape_and_decode(&xml_reader).unwrap();
                        }
                        CaptureNode::Description => {
                            description = e.unescape_and_decode(&xml_reader).unwrap()
                        }
                        CaptureNode::Url => url = e.unescape_and_decode(&xml_reader).unwrap(),
                    },
                    Ok(fast_xml::events::Event::Eof) => {
                        println!("Parsing finished");
                        break;
                    }
                    Err(e) => panic!(
                        "Error at position {}: {:?}",
                        xml_reader.buffer_position(),
                        e
                    ),
                    _ => {}
                }

                buf.clear();
            }

            let end = Instant::now();
            println!("Parsing took: {:?}", end - start);

            let start = Instant::now();
            print!("Saving index file...");
            std::io::stdout().flush().ok();
            let file = match std::fs::File::create(index_filename) {
                Ok(file) => file,
                Err(e) => {
                    println!("failed: {}. ", e.to_string());
                    return;
                }
            };

            let mut serializer = rmp_serde::Serializer::new(io::BufWriter::new(file));
            match index.serialize(&mut serializer) {
                Ok(_) => {
                    let end = Instant::now();
                    println!("succeeded. {:?}", end - start);
                }
                Err(e) => {
                    println!("failed: {}. ", e.to_string());
                    return;
                }
            }

            index
        }
    };

    let start = Instant::now();
    println!(
        "result: {:?}",
        index.search("London beer".to_string(), SearchType::AND)
    );
    let end = Instant::now();
    println!("Search: {:?}", end - start);

    let mut line = String::new();
    std::io::stdin().read_line(&mut line).unwrap();
}
