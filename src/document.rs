use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::analysis;

#[derive(Serialize, Deserialize, Debug)]
pub struct Document {
    pub id: i32,
    title: String,
    url: String,
    description: String,
    pub term_frequencies: HashMap<String, i32>,
}

impl Document {
    pub fn new(id: i32, title: String, url: String, description: String) -> Self {
        let mut doc = Self {
            id,
            title,
            url,
            description,
            term_frequencies: HashMap::new(),
        };

        doc.analyze();

        doc
    }

    fn analyze(&mut self) {
        analysis::analyze(&self.fulltext())
            .drain(0..)
            .for_each(|token| {
                let count = self.term_frequencies.entry(token).or_insert(0);
                *count += 1;
            });
    }

    fn fulltext(&self) -> String {
        [self.title.clone(), self.description.clone()].join(" ")
    }
}
