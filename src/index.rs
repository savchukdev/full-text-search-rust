use std::collections::{HashMap, HashSet};

use crate::analysis::analyze;
use crate::document::Document;

use serde_derive::{Deserialize, Serialize};

/// Index has to hash tables.
/// The first one, index, maps terms to document ids.
/// The second one, documents, maps document ids to corresponding documents.
/// With that, given a search term we can find all document ids of documents that contain that
/// term.
/// When there a multiple words in a search query, we will collect sets of document ids
/// and then either do union or intersection on those. Union will produce relaxed results,
/// where not necessarily all words in a query will be present in a document,
/// while intersection will only return documents that contain all the words in the query.
#[derive(Debug, Serialize, Deserialize)]
pub struct Index {
    /// map a term to an array of document ids
    /// it doesn't have to be hashset here, but it's more convenient later on
    /// during the search stage
    pub index: HashMap<String, HashSet<i32>>,
    /// map document id to a document
    pub documents: HashMap<i32, Document>,
}

#[allow(dead_code)]
pub enum SearchType {
    AND,
    OR,
}

impl Index {
    pub fn new() -> Self {
        Self {
            index: HashMap::new(),
            documents: HashMap::new(),
        }
    }

    pub fn index_document(&mut self, doc: Document) {
        for (term, _) in &doc.term_frequencies {
            let doc_ids = self.index.entry(term.clone()).or_insert(HashSet::new());
            doc_ids.insert(doc.id);
        }

        if !self.documents.contains_key(&doc.id) {
            self.documents.insert(doc.id, doc);
        }
    }

    // TODO: finish ranked search
    pub fn search(&self, query: String, search_type: SearchType) -> Vec<&Document> {
        let tokens = analyze(&query);

        let doc_ids_sets = tokens
            .iter()
            .filter_map(|token| self.index.get(token))
            .map(|ids| ids)
            .collect::<Vec<&HashSet<i32>>>();

        let doc_ids = match search_type {
            SearchType::AND => {
                let first = (*doc_ids_sets.get(0).unwrap_or(&&HashSet::new())).clone();

                doc_ids_sets
                .iter()
                .fold(first, |acc: HashSet<i32>, x| {
                    let intersection = acc.intersection(&x).into_iter();
                    HashSet::from_iter(intersection.copied())
                })
            }
            SearchType::OR => doc_ids_sets
                .iter()
                .fold(HashSet::new(), |acc: HashSet<i32>, x| {
                    let intersection = acc.union(&x).into_iter();
                    HashSet::from_iter(intersection.copied())
                }),
        };

        doc_ids
            .iter()
            .filter_map(|id| self.documents.get(id))
            .collect()
    }
}
