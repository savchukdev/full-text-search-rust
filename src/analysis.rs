use std::{cell::RefCell, collections::HashSet};

use lazy_static::lazy_static;
use regex::{escape, Regex};
use stemmer::Stemmer;

const STOPWORDS: [&'static str; 26] = [
    "the",
    "be",
    "to",
    "of",
    "and",
    "a",
    "in",
    "that",
    "have",
    "I",
    "it",
    "for",
    "not",
    "on",
    "with",
    "he",
    "as",
    "you",
    "do",
    "at",
    "this",
    "but",
    "his",
    "by",
    "from",
    "wikipedia",
];

lazy_static! {
    static ref PUNCTUATION: Regex = {
        let mut punctuation = ('!'..='/').collect::<HashSet<_>>();
        punctuation.extend((':'..='@').into_iter());
        punctuation.extend(('['..='`').into_iter());
        punctuation.extend(('{'..='~').into_iter());
        punctuation.insert('.');
        punctuation.insert(',');

        let escaped = escape(&punctuation.iter().collect::<String>());
        Regex::new(&escaped).expect("Punctuation must be valid")
    };
}

thread_local! {
    static STEMMER: RefCell<Stemmer> = {
        RefCell::new(Stemmer::new("english").expect("English stemmer must be there"))
    };
}

pub fn analyze(string: &String) -> Vec<String> {
    let tokens = string.split(" ");
    let tokens = tokens.map(|token| token.to_lowercase());
    let tokens = tokens.map(|token| PUNCTUATION.replace_all(&token, r"").into_owned());
    let tokens = tokens.filter(|token| !STOPWORDS.contains(&&token[..]));

    STEMMER.with(|s| {
        let mut stemmer = s.borrow_mut();
        tokens
            .map(move |token| stemmer.stem_str(&token[..]).to_string())
            .collect()
    })
}
